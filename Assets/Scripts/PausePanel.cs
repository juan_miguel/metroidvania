﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PausePanel : MonoBehaviour
{
    public GameObject pausePanel;
    private bool isPaused = false;

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            TogglePause();
        }
    }

    void TogglePause()
    {
        isPaused = !isPaused;

        if (isPaused)
        {
            PauseGame();
        }
        else
        {
            ResumeGame();
        }
    }

    void PauseGame()
    {
        Time.timeScale = 0f; // Pausa el tiempo del juego
        pausePanel.SetActive(true); // Activa el panel de pausa
    }

    void ResumeGame()
    {
        Time.timeScale = 1f; // Continúa el tiempo del juego
        pausePanel.SetActive(false); // Desactiva el panel de pausa
    }
}
