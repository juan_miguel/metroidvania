﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{
    [Header("Player")]
    public int lifes = 3;
    public int maxHealth = 100;
    public int maxEnergy = 100;
    public int currentHealth;
    public int currentEnergy;
    public int healthCosumed = 25;
    public int energyCosumed = 25;
    private float energyTime;
    public float energyTimer;
    public int score;
    public int gold;
    public int gems;
    public int ammunition1;
    public int ammunition2;
    public int ammunition3;
    private Slider healthSlider;
    private Slider energySlider;
    private Text healthText;
    private Text energyText;
    private Text lifesText;
    private Text ammunition1Text;
    private Text ammunition2Text;
    private Text ammunition3Text;
    private Image playerImage;
    public Sprite mainFace;
    public Sprite heatlhRecoverFace;
    public Sprite energyRecoverFace;
    public Sprite hurtFace;
    public Sprite tiredFace;
    /*********************************************************************************************/
    private bool recoveringHealth = false;
    /*********************************************************************************************/

    [Header("Enemy")]
    private GameObject enemy;
    private Slider enemyHealthSlider;
    private Text enemyHealthText;
    private Text enemyNameText;
    private Image enemyImage;

    [Header("NPC")]
    private GameObject npc;
    private Text npcNameText;
    private Image npcImage;

    [Header("Icons")]
    public Sprite[] meleeSprites;
    private Image meleeImage;
    private int meleeIndex = 0;
    public Sprite[] munitionSprites;

    
    void Start()
    {
        healthSlider = GameObject.Find("HealthBar").GetComponent<Slider>();
        energySlider = GameObject.Find("EnergyBar").GetComponent<Slider>();
        healthText = healthSlider.GetComponentInChildren<Text>();
        energyText = energySlider.GetComponentInChildren<Text>();
        lifesText = GameObject.Find("LifeText").GetComponent<Text>();
        playerImage = GameObject.Find("PlayerImage").GetComponent<Image>();
        currentHealth = maxHealth;
        currentEnergy = maxEnergy;
        healthText.text = currentHealth + "/" + maxHealth;
        energyText.text = currentHealth + "/" + maxHealth;
        lifesText.text = lifes.ToString();

        enemy = GameObject.Find("Enemy");
        enemyHealthSlider = enemy.GetComponentInChildren<Slider>();
        enemyHealthText = enemyHealthSlider.GetComponentInChildren<Text>();
        enemyNameText = GameObject.Find("EnemyText").GetComponent<Text>();
        enemyImage = GameObject.Find("EnemyImage").GetComponent<Image>();
        enemy.SetActive(false);

        npc = GameObject.Find("NPC");
        npcNameText = GameObject.Find("NPCText").GetComponent<Text>();
        npcImage = GameObject.Find("NPCImage").GetComponent<Image>();
        npc.SetActive(false);

        meleeImage = GameObject.Find("MeleeAttackImage").GetComponent<Image>();
        meleeImage.sprite = meleeSprites[meleeIndex];
        ammunition1Text = GameObject.Find("Ammunition1Text").GetComponent<Text>();
        ammunition2Text = GameObject.Find("Ammunition2Text").GetComponent<Text>();
        ammunition3Text = GameObject.Find("Ammunition3Text").GetComponent<Text>();

    }
    private void Update()
    {
        if (currentEnergy < maxEnergy) { ResetEnergy(); }

        /*********************************************************************************************/
        if (recoveringHealth)
        {
           // recoveryTime += Time.deltaTime;

           // if (recoveryTime >= recoveryTimer)
            {
                energySlider.value = maxEnergy;
                currentEnergy = maxEnergy;
                energyText.text = currentEnergy + "/" + maxEnergy;
                energyTime = 0;
            }
        }
        /*********************************************************************************************/
    }

    public void ConsumeHealth()
    {
        if (healthSlider.value > healthSlider.minValue) { 
            healthSlider.value = healthSlider.value - healthCosumed;
            currentHealth = currentHealth - healthCosumed;
        }
        else
        {
            lifes--;
            lifesText.text = lifes.ToString();
            healthSlider.value = healthSlider.maxValue;
            currentHealth = maxHealth;
            if(lifes == 1)
            {
                playerImage.sprite = tiredFace;
            }
        }
        healthText.text = currentHealth + "/" + maxHealth;
    }

    public void ConsumeEnergy()
    {
        if (energySlider.value > energySlider.minValue) {
            energySlider.value = energySlider.value - energyCosumed;
            currentEnergy = currentEnergy - energyCosumed;
            energyText.text = currentEnergy + "/" + maxEnergy;
        }
    }

    void ResetEnergy()
    {
        energyTime += Time.deltaTime;

        if (energyTime >= energyTimer)
        {
            energySlider.value = maxEnergy;
            currentEnergy = maxEnergy;
            energyText.text = currentEnergy + "/" + maxEnergy;
            energyTime = 0;
        }
    }

    /*********************************************************************************************/
    public void RecoverHealth()
    {
        recoveringHealth = true;
    }

    public void RecoverEnergy()
    {
       
    }
    /*********************************************************************************************/
    public void ChangePlayerFace()
    {
        if(playerImage.sprite == mainFace)
        {
            playerImage.sprite = hurtFace;
        }
        else
        {
            playerImage.sprite = mainFace;
        }
    }


    public void EnabledEnemyCanvas(int he, int dmg, int maxH, string name, Sprite face) 
    {
        enemyImage.sprite = face;
        enemyNameText.text = name;
        enemy.SetActive(true);
        enemyHealthSlider.value = he - dmg;
        he = he - dmg;
        enemyHealthText.text = he + "/" + maxH;
    }

    public void DisabledEnemyCanvas() 
    {
        enemy.SetActive(false);
    }

    public void EnabledNPCCanvas(string name, Sprite face)
    {
        npcImage.sprite = face;
        npcNameText.text = name;
        npc.SetActive(true);
    }

    public void DisabledNPCCanvas()
    {
        npc.SetActive(false);
    }

    public void NextMeleeAttack() 
    {
        if (meleeIndex == meleeSprites.Length-1)
        {
            meleeIndex = -1;
        }
        meleeIndex++;
        meleeImage.sprite = meleeSprites[meleeIndex];

    }

    public void GetAmmunition(string ammunitionType) 
    {
        switch (ammunitionType)
        {
            case "Ammunition1":
                ammunition1++;
                ammunition1Text.text = ammunition1.ToString();
                break;
            case "Ammunition2":
                ammunition2++;
                ammunition2Text.text = ammunition2.ToString();
                break;
            case "Ammunition3":
                ammunition3++;
                ammunition3Text.text = ammunition3.ToString();
                break;
        }
    }
}
