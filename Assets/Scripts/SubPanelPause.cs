﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;

public class SubPanelPause : MonoBehaviour
{   
    //Referencia al panel que quieres mostrar/ocultar
    public GameObject panel;
    
    //Variable para controlar el estado del panel
    private bool panelActive = false; 

    public void TogglePanel()
    {   
        //Cambiar el estado del panel
        panelActive = !panelActive; 

        //Mostrar u ocultar el panel según el estado
        panel.SetActive(panelActive); 
    }
}
