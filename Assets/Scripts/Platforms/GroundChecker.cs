﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundChecker : MonoBehaviour
{
    private CharacterController parent;

    private void Start()
    {
        parent = GetComponentInParent<CharacterController>();    
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            parent.Grounding();
        }
        if (collision.transform.tag == "Platform")
        {
            parent.Grounding();
            parent.gameObject.transform.SetParent(collision.transform);
        }
        if (collision.transform.tag == "JumpingPlatform" && parent.enablePlatform)
        {
            parent.Grounding();
            float jumpForce = collision.transform.GetComponent<JumpingPlatform>().jumpForce;
            parent.JumpWithPlatform(jumpForce, true);
        }
        if (collision.transform.tag == "Teleporter" && parent.enablePlatform)
        {
            parent.Grounding();
            parent.gameObject.transform.SetParent(collision.transform);
        }
        /*****************************************************************************************************************************/
        if(collision.transform.tag == "BreakablePlatform" && parent.enablePlatform)
        {
            parent.Grounding();
            collision.transform.GetComponent<BreakablePlatform>().startTimer = true;
        }
        /*****************************************************************************************************************************/
    }

    private void OnCollisionExit2D(Collision2D collision)
    {
        if (collision.transform.tag == "Platform" || collision.transform.tag == "Teleporter") 
        {
            parent.gameObject.transform.SetParent(null);
        }
        if (collision.transform.tag == "Ground")
        {
            parent.JumpWithPlatform(0, false);
        }
    }
}
