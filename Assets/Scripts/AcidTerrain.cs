﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AcidTerrain : MonoBehaviour
{
    //provoco un 25 de daño
    public float damagePercentage = 25f;

    //UI
    public UIController uiController;

    private void OnCollisionEnter2D(Collision2D collision)
    {   
        if (collision.gameObject.CompareTag("Player"))
        {
            uiController.ConsumeHealth();
        }
    }

    //Hacer que si está mucho rato vaya disminiyendo tmbn la vida

}
