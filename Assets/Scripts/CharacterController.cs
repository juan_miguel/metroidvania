﻿using System.Collections;
using System.Collections.Generic;
//using System.Diagnostics;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    [Header("Testing")]
    public float currentSpeed;

    [Header("UI")]
    private UIController uiController;

    [Header("Character Attributes")]
    private float blinkTimer;
    public float blinkTime;
    private float immuneTimer;
    public float immuneTime;
    private SpriteRenderer[] sr;
    public bool isImmune;

    [Header("Animators")]
    public Animator topAnimator;
    public Animator botAnimator;
    public Animator crouchAnimator;
    public Animator climbAnimator;
    public Animator fullAnimator;
    public Animator meleeAnimator;
    public Animator trepaAnimator;
    public Animator colgadoAnimator;

    //Movimiento
    [Header("Movement")]
    public float moveSpeed = 1;
    public float runSpeed = 2.5f;
    //private float currentSpeed;
    private Vector2 move;
    private bool lookingRight = true;
    private bool canMove;
    private bool isMoving;
    public bool canStandUp = true;
    private float changeDirTimer = 0;
    public bool canDecelerate = true;
    public bool canAccelerate;
    private float changeDirSpeed;

    //Salto
    [Header("Jump")]
    public float firstJumpForce;
    public float secondJumpForce;
    public bool canJump = false;
    public bool allowSecondJump = false;
    private int numOfJumps = 2;
    private float maxJumpSpeed = 7;
    private Rigidbody2D rb;
    private bool atGround;
    private bool isFalling;
    public float jumpTime;
    private float jumpTimer;
    private bool doJump;
    private bool heightDamage = false;
    

    //Posiciones de disparo
    [Header("Shoot Positions")]
    public Transform hShootPosition;
    public Transform vShootPosition;
    public Transform dShootPosition;
    public Transform hShootPositionCrouch;
    public Transform hShootPositionClimb;
    public Transform dUpShootPositionClimb;
    public Transform dDownShootPositionClimb;
    public Transform vDownShootPositionRoof;
    public Transform dDownShootPositionRoof;

    //Disparo
    [Header("Shoot")]
    public GameObject bullet;
    public float shootDelay;
    private float shootDelayTimer;
    private bool canShoot = true;
    public float delayToMove;
    public float delayToMoveTimer;
    private Vector2 shootDirection;
    private float delayToIdleTimer;
    private bool isShooting;

    //Crouch
    [Header("Crouch")]
    public float crouchMoveSpeed;
    public bool isCrouching;
    private float crouchValue;

    //Desplazamiento
    [Header("Dash")]
    public float dashDistance;
    public float dashSpeed;
    private Vector2 dashPosition;
    private bool doDash;
    public bool canDash;
    private bool isDashing;

    //Climb
    [Header("Climb")]
    public float climbMoveSpeed;
    public bool isClimbing = false;
    public bool isWallTouching = false;
    public bool isRoofTouching = false;
    public bool isVineTouching = false;
    public bool isWallClimbing = false;
    public bool isRoofClimbing = false;
    public bool isRoofCrouch = false;

    //Ataque melee
    [Header("Melee")]
    public string[] attack = {
        "Player_Melee_puñetazo",
        "Player_Melee_Espada",
        "Player_Melee_Cadena",
        "Plaer_Melee_Hammer",
        "Player_Melee_Escudo",
        "Player_Melee_Batcuerda_INICIO",
        "",                                 //Animación de la otra cuerda
    };
    public int numAttack = 0;
    private float attackTimer = 0;
    public bool isAttacking;
    public bool canAttack;
    public int attackDamage = 1;

    [Header("Hook")]
    private RaycastHit2D hit;
    public LineRenderer line;
    public DistanceJoint2D joint;
    private Transform hookOrigin;
    public bool isAiming;
    public bool isHooking;
    public bool stopHook = false;

    [Header("Rope")]
    public GameObject shootRope;
    public GameObject anchor;
    public GameObject rope;
    private Vector2 ropeDirection;
    public bool isAimingRope;
    private float loadRopeTimer;
    public float loadRopeTime;
    private int ropeLength = 1;
    public int maxRopeLength;

    [Header("Items")]
    public int armorItems;
    public int weaponItems;
    public int ammunitionItems;
    public int meleeItems;
    public int consumableItems;
    public int abilityItems;
    public int missionItmes;

    [Header("Sprites")]
    //Cambios sprites
    public GameObject fullSprite;
    public GameObject normalSprite;
    public GameObject crouchSprite;
    public GameObject dashSprite;
    public GameObject climbSprite;
    public GameObject meleeSprite;
    public GameObject trepaSprite;
    public GameObject colgadoSprite;

    private PlayerControls controls;
    public bool enablePlatform;

    void Awake()
    {
        controls = new PlayerControls();

        //controls.Gameplay.Jump.started += ctx => Jump();
        controls.Gameplay.Jump.performed += ctx => CheckJump();
        controls.Gameplay.Jump.canceled += ctx => CheckSecondJump();
        controls.Gameplay.Jump.canceled += ctx => doJump = false;
        controls.Gameplay.Shoot.performed += ctx => CheckShootDirection();
        controls.Gameplay.Crouch.performed += ctx => crouchValue = ctx.ReadValue<float>();
        controls.Gameplay.Crouch.performed += ctx => Crouch();
        controls.Gameplay.Crouch.canceled += ctx => CheckTop();
        controls.Gameplay.Dash.performed += ctx => Dash();
        controls.Gameplay.Run.performed += ctx => Run();
        controls.Gameplay.Run.canceled += ctx => currentSpeed = moveSpeed;
        controls.Gameplay.Move.performed += ctx => move = ctx.ReadValue<Vector2>();
        controls.Gameplay.Move.canceled += ctx => move = Vector2.zero;
        controls.Gameplay.ShootDirection.performed += ctx => shootDirection = ctx.ReadValue<Vector2>();
        controls.Gameplay.ShootDirection.canceled += ctx => shootDirection = Vector2.zero;
        controls.Gameplay.Climb.performed += ctx => Climb();
        controls.Gameplay.Attack.performed += ctx => Attack();
        controls.Gameplay.Attack.canceled += ctx => CheckEndAttack();
        controls.Gameplay.ChangeAttack.performed += ctx => ChangeAttack();
        controls.Gameplay.RoofCrouch.performed += ctx => RoofCrouch();
        controls.Gameplay.RoofCrouch.canceled += ctx => StopRoofCrouch();
        controls.Gameplay.AttackDirection.performed += ctx => ropeDirection = ctx.ReadValue<Vector2>();
    }

    private void Start()
    {
        uiController = GameObject.Find("UI").GetComponentInChildren<UIController>();
        rb = GetComponent<Rigidbody2D>();
        currentSpeed = moveSpeed;
        changeDirSpeed = runSpeed;
        canMove = true;
        canAttack = true;
        line.enabled = false;
        joint.enabled = false;
    }

    private void Update()
    {

        if (isVineTouching)
        {
            Climb();
        }

        //Apuntar y disparar cuerda
        if (isAiming)
        {
            CheckShootDirection();
        } 
        else if (isHooking)
        {
            if (hit.collider != null)
            {
                joint.enabled = true;
                line.SetPosition(0, hookOrigin.position);
                line.SetPosition(1, hit.point);
                joint.connectedAnchor = hit.point;

                if (stopHook)
                {
                    line.enabled = false;
                    joint.enabled = false;
                    isHooking = false;
                    stopHook = false;
                }
            }
            else
            {
                isHooking = false;
            }
        }

        if (isAimingRope)
        {
            AimRope();
        }

        if (isImmune) { DamageBlink(); }

        ChangeWallRoofClimb();

        if (isAttacking)
        {
            CheckEndLoadAttack();
        }

        //Salto
        if (canJump) { Jump(); }

        //Retardo de disparo.
        if (!canShoot)
        {
            shootDelayTimer += Time.deltaTime;
            if (shootDelayTimer >= shootDelay)
            {
                shootDelayTimer = 0;
                canShoot = true;
            }
        }

        //Sincronizacion de la animacion Idle
        if (isShooting)
        {
            delayToIdleTimer += Time.deltaTime;
            if (delayToIdleTimer >= topAnimator.GetCurrentAnimatorStateInfo(0).length)
            {
                delayToIdleTimer = 0;
                isShooting = false;
                botAnimator.Play("Player_Legs_IDLE", -1, 0f);
            }
        }

        if (isCrouching || isClimbing && !canMove)
        {
            delayToMoveTimer += Time.deltaTime;
            if (delayToMoveTimer >= delayToMove)
            {
                delayToMoveTimer = 0;
                canMove = true;
            }
        }

        //Sensor de caida.
        if (rb.velocity.y < 0) {
            if (!heightDamage && rb.velocity.y < -maxJumpSpeed)
            {
                heightDamage = true;
                uiController.ConsumeHealth();
            }
            
            Fall();
        }

        //Dash
        if (doDash)
        {
            canDash = false;
            canShoot = false;
            canJump = false;
            canAttack = false;
            transform.position = Vector3.MoveTowards(transform.position, dashPosition, dashSpeed * Time.deltaTime);
            if (transform.position.x == dashPosition.x)
            {
                isDashing = false;
                doDash = false;
                canMove = true;
                canDash = true;
                canShoot = true;
                canJump = true;
                canAttack = true;
                CheckTop();
            }
        }

        if (canMove)
        {
            if (move.x < 0)
            {
                if (lookingRight && (currentSpeed == runSpeed || currentSpeed == changeDirSpeed))
                {
                    ChangeRunDirection();
                }
                else
                {
                    move = new Vector2(-1, 0);
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                    lookingRight = false;
                }
            }
            else if (move.x > 0)
            {
                if (!lookingRight && (currentSpeed == runSpeed || currentSpeed == changeDirSpeed))
                {
                    ChangeRunDirection();
                }
                else
                {
                    move = new Vector2(1, 0);
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                    lookingRight = true;
                }
            }
            else if (move.y > 0 && isClimbing && !isRoofTouching)
            {
                move = new Vector2(0, 1);
            }
            else if (move.y < 0 && isClimbing && !isRoofTouching)
            {
                move = new Vector2(0, -1);
            }
            else
            {
                isMoving = false;
                if (isCrouching)
                {
                    crouchAnimator.SetBool("Walk", false);
                }
                else if (isClimbing)
                {
                    if (isVineTouching)
                    {
                        trepaAnimator.SetBool("Walk", false);
                    }
                    else if (isRoofTouching && isRoofCrouch)
                    {
                        colgadoAnimator.SetBool("CrouchWalk", false);
                    }
                    else if (isRoofTouching && !isRoofCrouch)
                    {
                        colgadoAnimator.SetBool("Walk", false);
                    }
                    else
                    {
                        climbAnimator.SetBool("WalkUp", false);
                        climbAnimator.SetBool("WalkDown", false);
                    }
                }
                else
                {
                    topAnimator.SetBool("Walk", false);
                    botAnimator.SetBool("Walk", false);
                    topAnimator.SetBool("Run", false);
                    botAnimator.SetBool("Run", false);
                }
                return;
            }

            Vector2 c = new Vector2(0, move.y) * currentSpeed * Time.deltaTime;
            transform.Translate(c, Space.World);

            Vector2 m = new Vector2(move.x, 0) * currentSpeed * Time.deltaTime;
            transform.Translate(m, Space.World);
            isMoving = true;


            //Animaciones de movimiento.
            if (isCrouching)
            {
                crouchAnimator.SetBool("Walk", true);
            }
            else if (isClimbing && move.y > 0)
            {
                if (isVineTouching)
                {
                    trepaAnimator.SetBool("Walk", true);
                }
                else
                {
                    climbAnimator.SetBool("WalkUp", true);
                    climbAnimator.SetBool("WalkDown", false);
                }
            }
            else if (isClimbing && move.y < 0)
            {
                if (isVineTouching)
                {
                    trepaAnimator.SetBool("Walk", true);
                }
                else
                {
                    climbAnimator.SetBool("WalkUp", false);
                    climbAnimator.SetBool("WalkDown", true);
                }

            }
            else if (isClimbing && isRoofTouching && isRoofCrouch)
            {
                colgadoAnimator.SetBool("CrouchWalk", true);
            }
            else if (isClimbing && isRoofTouching && !isRoofCrouch)
            {
                colgadoAnimator.SetBool("Walk", true);
            }
            else if (currentSpeed == moveSpeed)
            {
                topAnimator.SetBool("Walk", true);
                botAnimator.SetBool("Walk", true);
                topAnimator.SetBool("Run", false);
                botAnimator.SetBool("Run", false);
            }
            else if (currentSpeed == runSpeed || canDecelerate || canAccelerate)
            {
                topAnimator.SetBool("Walk", false);
                botAnimator.SetBool("Walk", false);
                topAnimator.SetBool("Run", true);
                botAnimator.SetBool("Run", true);
            }
        }

        if (!isWallTouching && !isRoofTouching && !isVineTouching)
        {
            if (isClimbing)
            {
                climbSprite.SetActive(false);
                trepaSprite.SetActive(false);
                colgadoSprite.SetActive(false);
                normalSprite.SetActive(true);
                currentSpeed = moveSpeed;
            }
            rb.gravityScale = 1;
            isClimbing = false;
        }
    }

    //Salto
    void Jump()
    {
        if (doJump)
        {
            if (isClimbing)
            {
                rb.gravityScale = 1;
                climbSprite.SetActive(false);
                trepaSprite.SetActive(false);
                colgadoSprite.SetActive(false);
                normalSprite.SetActive(true);
                isClimbing = false;
                currentSpeed = moveSpeed;
            }
            else if (isAttacking)
            {
                meleeSprite.SetActive(false);
                normalSprite.SetActive(true);
                isAttacking = false;
                canMove = true;
            }
            enablePlatform = false;
            atGround = false;
            canDash = false;
            canAttack = false;
            numOfJumps = 1;

            //rb.velocity = Vector2.zero;
            jumpTimer += Time.deltaTime;
            if (jumpTimer >= jumpTime) { doJump = false; }
            else
            {
                rb.velocity = Vector2.zero;
                rb.AddForce(new Vector2(0, firstJumpForce), ForceMode2D.Impulse);
            }

            topAnimator.Play("Jump");
            botAnimator.Play("Jump");
            topAnimator.SetBool("Land", false);
            botAnimator.SetBool("Land", false);
        }
    }

    //Doble salto
    void SecondJump()
    {
        if (canJump && numOfJumps == 1 && allowSecondJump)
        {
            enablePlatform = false;
            allowSecondJump = false;
            canJump = false;
            numOfJumps = 0;

            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, secondJumpForce), ForceMode2D.Impulse);
            isFalling = false;

            botAnimator.Play("DoubleJump");
        }
    }

    //Caida
    void Fall()
    {
        if (!isFalling)
        {
            if (isClimbing)
            {
                dashSprite.SetActive(false);
                climbSprite.SetActive(false);
                trepaSprite.SetActive(false);
                colgadoSprite.SetActive(false);
                crouchSprite.SetActive(false);
                normalSprite.SetActive(true);
                isClimbing = false;
                currentSpeed = moveSpeed;
            }

            

            isFalling = true;
            atGround = false;
            canDash = false;
            canAttack = false;
            enablePlatform = true;

            topAnimator.SetBool("Land", false);
            botAnimator.SetBool("Land", false);
            topAnimator.SetBool("Falling", true);
            botAnimator.SetBool("Falling", true);
        }
    }

    //Restablece los saltos
    void RestartJump()
    {
        rb.velocity = Vector2.zero;
        numOfJumps = 2;
        allowSecondJump = false;
        jumpTimer = 0;
        if (!isCrouching) { canJump = true; }
    }

    //Acciones al tocar el suelo
    public void Grounding()
    {
        rb.velocity = Vector2.zero;
        RestartJump();
        atGround = true;
        canStandUp = true;
        canDash = true;
        canAttack = true;
        isFalling = false;
        heightDamage = false;

        topAnimator.SetBool("Land", true);
        botAnimator.SetBool("Land", true);
        topAnimator.SetBool("Falling", false);
        botAnimator.SetBool("Falling", false);
    }

    void CheckJump()
    {
        if (atGround || isClimbing)
        {
            doJump = true;
            //canJump = true; 
        }
        else { SecondJump(); }
    }

    //Comprueba si puede ejecutar el segundo salto
    void CheckSecondJump()
    {
        if (rb.velocity.y != 0) { allowSecondJump = true; }
    }

    public void JumpWithPlatform(float jumpForce, bool platform)
    {
        if (platform)
        {
            rb.velocity = Vector2.zero;
            rb.AddForce(new Vector2(0, jumpForce), ForceMode2D.Impulse);
            atGround = false;
            topAnimator.Play("Jump");
            botAnimator.Play("Jump");
            topAnimator.SetBool("Land", false);
            botAnimator.SetBool("Land", false);
        }
        numOfJumps = 1;
        allowSecondJump = true;
    }

    //Disparo en horizontal
    void HorizontalShoot()
    {
        if (isAiming)
        {
            ShootHook(hShootPosition);
        }
        else if (canShoot)
        {
            if (isCrouching)
            {
                crouchAnimator.Play("HorizontalShoot", -1, 0);
                Instantiate(bullet, hShootPositionCrouch.position, hShootPositionCrouch.rotation);
                canMove = false;
                delayToMoveTimer = 0;
            }
            else if (isClimbing)
            {
                if (isRoofTouching && isMoving)
                {
                    colgadoAnimator.Play("Player_Colgado_Move_Shoot_Horizontal", -1, 0);
                    Instantiate(bullet, hShootPosition.position, hShootPosition.rotation);
                }
                else if (isRoofTouching && !isMoving)
                {
                    colgadoAnimator.Play("Player_Colgado_Shoot_Horizontal", -1, 0);
                    Instantiate(bullet, hShootPosition.position, hShootPosition.rotation);
                }
                else
                { 
                    climbAnimator.Play("Player_Full_Clib_Shoot", -1, 0);
                    Instantiate(bullet, hShootPositionClimb.position, hShootPositionClimb.rotation);
                    canMove = false;
                    delayToMoveTimer = 0;
                }
            }
            else
            {
                topAnimator.Play("HorizontalShoot", -1, 0);
                isShooting = true;
                if (atGround && !isMoving)
                {
                    botAnimator.Play("Shoot", -1, 0);
                }
                Instantiate(bullet, hShootPosition.position, hShootPosition.rotation);
            }
            canShoot = false;
        }
    }

    //Disparo en vertical
    void VerticalShoot()
    {
        if (isAiming)
        {
            ShootHook(vShootPosition);
        }
        else if (canShoot && !isCrouching && !isClimbing)
        {
            topAnimator.Play("VerticalShoot", -1, 0);
            if (atGround && !isMoving)
            {
                botAnimator.Play("Shoot", -1, 0);
            }

            Instantiate(bullet, vShootPosition.position, vShootPosition.rotation);
            canShoot = false;
        }
    }

    //Disparo en diagonal hacia arriba
    void UpDiagonalShoot()
    {
        if (isAiming)
        {
            ShootHook(dShootPosition);
        }
        else if (canShoot && !isCrouching && !isRoofCrouch)
        {
            if (isClimbing)
            {
                climbAnimator.Play("player_Full_Climb_Shoot_Diagonal_UP", -1, 0);
                Instantiate(bullet, dUpShootPositionClimb.position, dUpShootPositionClimb.rotation);
                canMove = false;
                delayToMoveTimer = 0;
            }
            else
            {

                topAnimator.Play("DiagonalShoot", -1, 0);
                isShooting = true;
                if (atGround && !isMoving)
                {
                    botAnimator.Play("Shoot", -1, 0);
                }

                Instantiate(bullet, dShootPosition.position, dShootPosition.rotation);
                canShoot = false;
            }
        }
    }

    //Disparo en diagonal hacia abajo
    void DownDiagonalShoot()
    {
        if (canShoot && isClimbing)
        {
            if (isRoofTouching && isMoving)
            {
                colgadoAnimator.Play("Player_Colgado_Move_Shoot_Diagonal", -1, 0);
                Instantiate(bullet, dDownShootPositionRoof.position, dDownShootPositionRoof.rotation);
            }
            else if (isRoofTouching && !isMoving)
            {
                colgadoAnimator.Play("Player_Colgado_Shoot_Down", -1, 0);
                Instantiate(bullet, vDownShootPositionRoof.position, vDownShootPositionRoof.rotation);
            }
            else
            {
                climbAnimator.Play("player_Full_Climb_Shoot_Diagonal_Down", -1, 0);
                Instantiate(bullet, dDownShootPositionClimb.position, dDownShootPositionClimb.rotation);
                canMove = false;
                delayToMoveTimer = 0;
            }
        }
    }

    //Comprueba la direccion de disparo
    void CheckShootDirection()
    {
        if (isAttacking && !isAiming)
        {
            meleeSprite.SetActive(false);
            normalSprite.SetActive(true);
            isAttacking = false;
            topAnimator.SetBool("Land", true);
            botAnimator.SetBool("Land", true);
        }

        if (shootDirection.y > 0)
        {
            if (shootDirection.x >= -0.5f && shootDirection.x <= 0.5f) { VerticalShoot(); }
            else { UpDiagonalShoot(); }
        }
        else if (shootDirection.y < 0 && !isCrouching)
        {
            DownDiagonalShoot();
        }
        else { HorizontalShoot(); }
    }


    //Disparar cuerda
    void ShootHook(Transform origin)
    {
        hit = Physics2D.Raycast(origin.position, origin.right.normalized, 2f);
        Debug.DrawLine(origin.position, origin.position + origin.right.normalized * 2f, Color.blue);

        if (hit.collider != null)
        {
            if (hit.collider.gameObject.CompareTag("Ground") || hit.collider.gameObject.CompareTag("Climb"))
            {
                hookOrigin = origin;
                line.enabled = true;
                line.SetPosition(0, origin.position);
                line.SetPosition(1, hit.point);
            }
        }
    }

    void Run()
    {
        if (!isCrouching) { currentSpeed = runSpeed; }
    }

    //Cambiar de direccion al corre
    void ChangeRunDirection()
    {
        if (canDecelerate)
        {
            canJump = false;
            canShoot = false;
            changeDirSpeed = changeDirSpeed - 1f;
            currentSpeed = changeDirSpeed;
            canDecelerate = false;
        }

        if (!canDecelerate && !canAccelerate)
        {
            normalSprite.SetActive(false);
            fullSprite.SetActive(true);
            if (changeDirTimer < fullAnimator.GetCurrentAnimatorStateInfo(0).length)
            {
                changeDirTimer += Time.deltaTime;
            }
            else
            {
                changeDirTimer = 0;
                fullSprite.SetActive(false);
                normalSprite.SetActive(true);
                if (lookingRight)
                {
                    transform.rotation = Quaternion.Euler(0, 180, 0);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(Vector3.zero);
                }

                canAccelerate = true;
            }
        }

        if (canAccelerate)
        {
            changeDirSpeed = changeDirSpeed + 1f;
            currentSpeed = changeDirSpeed;
            canAccelerate = false;
            canDecelerate = true;
            canJump = true;
            canShoot = true;
            lookingRight = !lookingRight;
        }
    }

    //Se agacha
    void Crouch()
    {
        if (!isDashing && !isClimbing && !isAttacking && atGround)
        {
            if (crouchValue > 0.35f || !canStandUp)
            {
                normalSprite.SetActive(false);
                dashSprite.SetActive(false);
                climbSprite.SetActive(false);
                trepaSprite.SetActive(false);
                colgadoSprite.SetActive(false);
                crouchSprite.SetActive(true);
                isCrouching = true;
                canJump = false;
                currentSpeed = crouchMoveSpeed;
            }
            //else { CheckTop(); }
        }
    }

    //Agacharse en el techo
    void RoofCrouch()
    {
        if(isClimbing && isRoofTouching)
        {
            isRoofCrouch = true;
            colgadoSprite.GetComponent<SpriteRenderer>().flipX = true;
            colgadoAnimator.SetBool("Crouch", true);
        }
    }

    //Dejar de agacharse en el techo
    void StopRoofCrouch()
    {
        if (isRoofCrouch)
        {
            isRoofCrouch = false;
            colgadoSprite.GetComponent<SpriteRenderer>().flipX = false;
            colgadoAnimator.SetBool("Crouch", false);
        }
    }

    //Calcula la posicion final para el desplazamiento
    void Dash()
    {
        if (canDash)
        {
            normalSprite.SetActive(false);
            crouchSprite.SetActive(false);
            climbSprite.SetActive(false);
            trepaSprite.SetActive(false);
            colgadoSprite.SetActive(false);
            meleeSprite.SetActive(false);
            dashSprite.SetActive(true);

            isAttacking = false;
            isDashing = true;
            doDash = true;
            canMove = false;
            canAttack = false;
            dashPosition = transform.position;
            if (lookingRight) { dashPosition.x += dashDistance; }
            else { dashPosition.x -= dashDistance; }
        }
    }

    //Escala
    void Climb()
    {
        if ((isWallTouching || isVineTouching) && !atGround || isRoofTouching)
        {
            normalSprite.SetActive(false);
            crouchSprite.SetActive(false);
            dashSprite.SetActive(false);
            if (isVineTouching)
                trepaSprite.SetActive(true);
            else if (isRoofTouching) {
                isRoofClimbing = true;
                colgadoSprite.SetActive(true);
            }
            else { 
                isWallClimbing = true;
                climbSprite.SetActive(true);
            }
            isClimbing = true;
            isFalling = false;
            RestartJump();
            canDash = false;
            canAttack = false;
            rb.velocity = Vector3.zero;
            rb.gravityScale = 0;
            currentSpeed = climbMoveSpeed;
        }
    }

    //Cambiar animación, colgarse o escalar
    void ChangeWallRoofClimb()
    {
        if (isWallClimbing && isRoofTouching)
        {
            isWallClimbing = false;
            isRoofClimbing = true;
            colgadoSprite.SetActive(true);
            climbSprite.SetActive(false);
            isWallTouching = false;

        }
        else if (isRoofClimbing && isWallTouching)
        {
            isWallClimbing = true;
            isRoofClimbing = false;
            colgadoSprite.SetActive(false);
            climbSprite.SetActive(true);
            isRoofTouching = false;
        }
    }

    void AimRope()
    {
        if (ropeDirection.y > 0)
        {
            if (ropeDirection.x >= -0.5f && ropeDirection.x <= 0.5f)
            {
                meleeAnimator.SetBool("LoadRopeD", false);
                meleeAnimator.SetBool("LoadRopeV", true);
            }

            else if (meleeAnimator.GetBool("LoadRopeV"))
            {
                meleeAnimator.SetBool("LoadRopeV", false);
                meleeAnimator.SetBool("LoadRopeD", true);
            }

           else if (meleeAnimator.GetBool("LoadRopeH"))
           {
                meleeAnimator.SetBool("LoadRopeH", false);
                meleeAnimator.SetBool("LoadRopeD", true);
            }  
        }
        else
        {
            meleeAnimator.SetBool("LoadRopeD", false);
            meleeAnimator.SetBool("LoadRopeH", true);
        }

        LoadRope();
    }

    //Cargar longitud cuerda
    void LoadRope()
    {
        if(uiController.currentEnergy > 0)
        {
            loadRopeTimer += Time.deltaTime;
            if (loadRopeTimer >= loadRopeTime)
            {
                if (ropeLength < maxRopeLength) { ropeLength++; }
                loadRopeTimer = 0;
                uiController.ConsumeEnergy();
            }
        }
    }

    //Ataque melee
    void Attack()
    {
        if (canAttack && uiController.currentEnergy > 0) {
            
            isAttacking = true;
            canMove = false;
            normalSprite.SetActive(false);
            crouchSprite.SetActive(false);
            dashSprite.SetActive(false);
            climbSprite.SetActive(false);
            trepaSprite.SetActive(false);
            colgadoSprite.SetActive(false);
            meleeSprite.SetActive(true);
            if (numAttack < attack.Length -1)
            {
                Debug.Log(attack[numAttack]);
                meleeAnimator.Play(attack[numAttack], -1, 0);
                uiController.ConsumeEnergy();
            }
            else if (numAttack == attack.Length - 1)
            {
                meleeAnimator.SetBool("ShootRope", false);
                isAimingRope = true;
                
                //Comprobar direccion batcuerda
                if (ropeDirection.y > 0)
                {
                    if (ropeDirection.x >= -0.5f && ropeDirection.x <= 0.5f) {
                        meleeAnimator.SetBool("LoadRopeV", true);
                    }
                    else {
                        meleeAnimator.SetBool("LoadRopeD", true);
                    }
                }
                else {
                    meleeAnimator.SetBool("LoadRopeH", true);
                }
            }
            else
            {
                isAiming = true;
                uiController.ConsumeEnergy();
            }
        }
    }

    //Comprobar si la animacion de ataque ha terminado
    void CheckEndLoadAttack()
    {
        if (!meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo") && 
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo_INICIO") && 
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo_ACTIVO") &&
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_INICIO") &&
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_Carga_Horizontal") &&
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Melee_Batcuerda_Carga_Diagonal") &&
            !meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_Carga_Arriba"))
        {
            if (attackTimer < meleeAnimator.GetCurrentAnimatorStateInfo(0).length)
            {
                attackTimer += Time.deltaTime;
            }
            else
            {
                EndAttack();
            }
        }
    }

    //Comprobar si la animacion de escudo o de cuerda ha terminado
    void CheckEndAttack()
    {
        if (isAiming)
        {
            isAiming = false;
            isHooking = true;
            EndAttack();
        }
        else
        {
            if (meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo") ||
                meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo_INICIO") ||
                meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Escudo_ACTIVO"))
            {
                meleeAnimator.SetBool("Shield_Off", true);
                if (attackTimer < meleeAnimator.GetCurrentAnimatorStateInfo(0).length)
                {
                    attackTimer += Time.deltaTime;
                }
                else
                {
                    EndAttack();
                }
            }
            else if (meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_INICIO") ||
                    meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_Carga_Horizontal") ||
                    meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Melee_Batcuerda_Carga_Diagonal") ||
                    meleeAnimator.GetCurrentAnimatorStateInfo(0).IsName("Player_Melee_Batcuerda_Carga_Arriba"))
            {
                
                isAimingRope = false;
                if (meleeAnimator.GetBool("LoadRopeH"))
                {
                    meleeAnimator.SetBool("LoadRopeH", false);
                    Instantiate(shootRope, hShootPosition.position, hShootPosition.rotation);
                }
                else if (meleeAnimator.GetBool("LoadRopeD"))
                {
                    meleeAnimator.SetBool("LoadRopeD", false);
                    Instantiate(shootRope, dShootPosition.position, dShootPosition.rotation);

                } else if (meleeAnimator.GetBool("LoadRopeV"))
                {
                    meleeAnimator.SetBool("LoadRopeV", false);
                    Instantiate(shootRope, vShootPosition.position, vShootPosition.rotation);
                }
                meleeAnimator.SetBool("ShootRope", true);
            }
        }     
    }

    //Finalizar ataque
    void EndAttack()
    {
        isAttacking = false;
        canAttack = true;
        canMove = true;
        attackTimer = 0;
        normalSprite.SetActive(true);
        crouchSprite.SetActive(false);
        dashSprite.SetActive(false);
        climbSprite.SetActive(false);
        trepaSprite.SetActive(false);
        colgadoSprite.SetActive(false);
        meleeSprite.SetActive(false);
        topAnimator.SetBool("Land", true);
        botAnimator.SetBool("Land", true);
    }

    //Cambiar ataque melee
    void ChangeAttack()
    {
        if (numAttack < attack.Length - 1)
        {
            numAttack++;
        }
        else
        {
            numAttack = 0;
        }

        uiController.NextMeleeAttack();
    }

    //Generar batcuerda
    public void CreateRope(Transform transform, string tag)    
    {
        GameObject anchorObject;

        if (tag == "Roof")
        {
            anchorObject = Instantiate(anchor, transform.position, anchor.transform.rotation);
            
        } else
        {
            Quaternion anchorRotation = Quaternion.AngleAxis(270, Vector3.forward);
            anchorObject = Instantiate(anchor, transform.position, anchorRotation);
        }

        GameObject ropeElement = Instantiate(rope, anchorObject.transform.position, rope.transform.rotation);
        for (int i=1; i<ropeLength; i++)
        {
            Vector3 position = new Vector3(ropeElement.transform.position.x, ropeElement.transform.position.y - (ropeElement.GetComponent<SpriteRenderer>().sprite.bounds.size.y), ropeElement.transform.position.z);
            ropeElement = Instantiate(rope, position, rope.transform.rotation);
            
        }
        ropeLength = 1;
    }

    void SwitchToNormalSprite()
    {
        if (!isDashing)
        {
            crouchValue = 0;
            canMove = true;
            canJump = true;
            canAttack = true;
            crouchSprite.SetActive(false);
            dashSprite.SetActive(false);
            climbSprite.SetActive(false);
            trepaSprite.SetActive(false);
            colgadoSprite.SetActive(false);
            normalSprite.SetActive(true);
            isCrouching = false;
            currentSpeed = moveSpeed;
            topAnimator.SetBool("Land", true);
            botAnimator.SetBool("Land", true);
        }
    }

    void CheckTop()
    {
        if (canStandUp && controls.Gameplay.Crouch.phase != InputActionPhase.Started && !isClimbing) { SwitchToNormalSprite(); }
        else { Crouch(); }
    }

    void ForceStopDash()
    {
        isDashing = false;
        doDash = false;
        canMove = true;
        canDash = true;
        canShoot = true;
        canJump = true;
        canStandUp = true;
        topAnimator.SetBool("Land", true);
        CheckTop();
    }

    void GetDamage()
    {
        sr = GetComponentsInChildren<SpriteRenderer>();
        uiController.ConsumeHealth();
        if(uiController.lifes <= 0 && uiController.currentHealth <= 0)
        {
            Destroy(gameObject);
        }
        else
        {
            uiController.ChangePlayerFace();
            isImmune = true;
            immuneTimer = 0;
            for(int i=0; i<sr.Length; i++)
            {
                sr[i].enabled = false;
            }  
            blinkTimer = 0;
        }
    }

    void DamageBlink()
    {
        sr = GetComponentsInChildren<SpriteRenderer>();
        immuneTimer += Time.deltaTime;
        blinkTimer += Time.deltaTime;

        if (blinkTimer >= blinkTime)
        {
            for (int i = 0; i < sr.Length; i++)
            {
                sr[i].enabled = !sr[i].enabled;
            }
            blinkTimer = 0;
        }

        if (immuneTimer >= immuneTime)
        {
            for (int i = 0; i < sr.Length; i++)
            {
                sr[i].enabled = true;
            }
            isImmune = false;
            uiController.ChangePlayerFace();
        }
    }

    private void OnEnable()
    {
        controls.Gameplay.Enable();
    }

    private void OnDisable()
    {
        controls.Gameplay.Disable();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            canStandUp = false;
            if (isHooking) { stopHook = true; }       
        }

        if (collision.transform.tag == "EnemyProjectile")
        {
            if (!isImmune) { GetDamage(); }
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "EnemyAttack")
        {
            if (!isImmune) { GetDamage(); }
        }

        if(collision.transform.tag == "Ammunition1" || collision.transform.tag == "Ammunition2" || collision.transform.tag == "Ammunition3")
        {
            Destroy(collision.gameObject);
            uiController.GetAmmunition(collision.transform.tag);
        }

        if (collision.gameObject.CompareTag("Treasure"))
        {
            collision.gameObject.GetComponent<Chest>().OpenChest();
        }


        if (collision.transform.tag == "Armor")
        {
            armorItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Weapon")
        {
            weaponItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Ammunition")
        {
            ammunitionItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Melee")
        {
            meleeItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Consumable")
        {
            consumableItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Ability")
        {
            abilityItems++;
            Destroy(collision.gameObject);
        }
        else if (collision.transform.tag == "Mission")
        {
            missionItmes++;
            Destroy(collision.gameObject);
        }
        /***************************************************************************************************************************************/
        else if (collision.transform.tag == "HealthPotion")
        {
            uiController.RecoverHealth();
            Destroy(collision.gameObject);
        }
        /***************************************************************************************************************************************/
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Ground")
        {
            if (isCrouching || isDashing)
            {
                canStandUp = true;
                CheckTop();
            }
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.transform.tag == "Ground" && isDashing)
        {
            ForceStopDash();
        }
    }
}
