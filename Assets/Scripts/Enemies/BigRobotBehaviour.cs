﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigRobotBehaviour : MonoBehaviour
{
    [Header("Robot Attributes")]
    public Sprite faceImage;
    public int health;
    public int maxHealth;
    public float moveSpeed;
    public float patrolSpeed;
    public float patrolTime;
    private float patrolTimer;
    public float aimTime;
    private float aimingTimer;
    public float refreshTime;
    private float refreshTimer;
    private float blinkTimer;
    public float blinkTime;
    private float immuneTimer;
    public float immuneTime;

    private SpriteRenderer sr;

    [Header("Sprites")]
    public GameObject explosion;

    [Header("IA")]
    public float playerStopDistance;
    public Transform playerTransform;
    private RobotRoofChecker childrenRoofC;
    private Vector3 initialPosition;

    [Header("UI")]
    public UIController uiController;

    [Header("Shoot")]
    public Transform shootPosition;
    public GameObject bullet;


    public bool isImmune;
    public bool hasObjective;
    public bool lookRight;
    public bool canShoot;
    public bool canPatrol = true;

    // Start is called before the first frame update
    void Start()
    {
        if (transform.rotation.y == 0) { lookRight = true; }
        else { lookRight = false; }

        childrenRoofC = GetComponentInChildren<RobotRoofChecker>();
        sr = GetComponent<SpriteRenderer>();
        uiController = GameObject.FindObjectOfType<UIController>();

        initialPosition = transform.position;      
    }

    // Update is called once per frame
    void Update()
    {
        if (!hasObjective && canPatrol) { Patrol(); }
        else if (!hasObjective && !canPatrol) { GoInitialPosition(); }
        
        if (hasObjective && !childrenRoofC.isRoof) { FollowPlayer(); }

        if (hasObjective && !canShoot) { Refresh(); }
        else if (canShoot) { Aim(); }

        if (isImmune) { DamageBlink(); }
    }

    //Disparo
    void Shoot()
    {
        Instantiate(bullet, shootPosition.position, shootPosition.rotation);
    }

    //Si pierde la visión del player, vuelve a su posicion inicial.
    void GoInitialPosition()
    {
        transform.position = Vector2.MoveTowards(transform.position, initialPosition, moveSpeed * Time.deltaTime);

        if (Vector2.Distance(transform.position, initialPosition) < 0.1) 
        {
            canPatrol = true;
            childrenRoofC.isRoof = false;
        }
    }

    //Si tiene vision del player lo persigue.
    void FollowPlayer()
    {
        canPatrol = false;

        if (Vector2.Distance(transform.position, playerTransform.position) > playerStopDistance)
        {
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(playerTransform.position.x, transform.position.y), moveSpeed * Time.deltaTime);
        }
    }

    //Si no tiene vision del player patrulla.
    void Patrol()
    {
        if (lookRight)
        {
            transform.Translate(Vector2.right * patrolSpeed * Time.deltaTime, Space.World);
        }
        else
        {
            transform.Translate(Vector2.left * patrolSpeed * Time.deltaTime, Space.World);
        }

        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolTime)
        {
            patrolTimer = 0;
            ChangeDirection();
        }
    }

    //Si tiene vision del player apunta durante un tiempo y despues dispara.
    void Aim()
    {
        aimingTimer += Time.deltaTime;
        if (aimingTimer >= aimTime)
        {
            aimingTimer = 0;
            Shoot();
            canShoot = false;
        }
    }

    //Rota para mirar hacia la otra dirección.
    void ChangeDirection()
    {
        if (lookRight)
        {
            transform.rotation = new Quaternion(0, 180, 0, 0);
            lookRight = false;
        }
        else
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
            lookRight = true;
        }
    }

    //Delay para reiniciar el funcionamiento del robot.
    void Refresh()
    {
        refreshTimer += Time.deltaTime;
        if (refreshTimer >= refreshTime)
        {
            refreshTimer = 0;
            canShoot = true;
        }
    }

    //Daña al robot al recibir disparos.
    void GetDamage(int dmg)
    {
        health -= dmg;
        //Añadir animación de daño

        if (health <= 0)
        {
            GameObject effect = Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(effect, 0.5f);
            uiController.DisabledEnemyCanvas();
        }
        else
        {
            isImmune = true;
            immuneTimer = 0;
            sr.enabled = false;
            blinkTimer = 0;
        }
    }

    //Si recibe daño, parpadea
    void DamageBlink()
    {
        immuneTimer += Time.deltaTime;
        blinkTimer += Time.deltaTime;

        if (blinkTimer >= blinkTime)
        {
            sr.enabled = !sr.enabled;
            blinkTimer = 0;
        }

        if (immuneTimer >= immuneTime)
        {
            sr.enabled = true;
            isImmune = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = true;
        }

        if (collision.transform.tag == "Projectile")
        {
            var dmg = collision.gameObject.GetComponent<Projectile>().damage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }       
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Attack")
        {
            var dmg = collision.gameObject.GetComponentInParent<CharacterController>().attackDamage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = false;
            ChangeDirection();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (collision.transform.position.x > transform.position.x)
            {
                if (!lookRight)
                {
                    ChangeDirection();
                }
            }
            else
            {
                if (lookRight)
                {
                    ChangeDirection();
                }
            }
        }
    }
}
