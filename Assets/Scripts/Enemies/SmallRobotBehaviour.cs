﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmallRobotBehaviour : MonoBehaviour
{
    [Header("Robot Attributes")]
    public Sprite faceImage;
    public int health;
    public int maxHealth;
    public float moveSpeed;
    public float patrolSpeed;
    public float patrolTime;
    private float patrolTimer;
    private float blinkTimer;
    public float blinkTime;
    private float immuneTimer;
    public float immuneTime;

    private SpriteRenderer sr;

    [Header("Sprites")]
    public GameObject explosion;

    [Header("IA")]
    private RobotGroundChecker childrenGroundC;
    private RobotClimbChecker childrenClimbC;
    public Transform playerTransform;

    [Header("UI")]
    public UIController uiController;

    public bool isImmune;
    public bool hasObjective;
    public bool lookRight;
    public bool lookUp;
    public bool faceDown;
    public bool canPatrol = true;
    public bool canClimb;

    //Posicion del robot en la plataforma
    private bool isUp = true;
    private bool isDown;
    private bool isRight;
    private bool isLeft;   

    //Posiciones donde buscar al player si el robot esta arriba
    private bool uSearchRight;
    private bool uSearchLeft;
    private bool uSearchRightDown;
    private bool uSearchLeftDown;

    //Posiciones donde buscar al player si el robot esta abajo
    private bool dSearchRight;
    private bool dSearchLeft;
    private bool dSearchRightUp;
    private bool dSearchLeftUp;

    //Posiciones donde buscar al player si el robot esta en el lateral derecho
    private bool rSearchUp;
    private bool rSearchDown;
    private bool rSearchUpLeft;
    private bool rSearchDownLeft;

    //Posiciones donde buscar al player si el robot esta en el laterala izquierdo
    private bool lSearchUp;
    private bool lSearchDown;
    private bool lSearchUpRight;
    private bool lSearchDownRight;

    void Start()
    {
        if (transform.rotation.y == 0) { lookRight = true; }
        else { lookRight = false; }

        childrenGroundC = GetComponentInChildren<RobotGroundChecker>();
        childrenClimbC = GetComponentInChildren<RobotClimbChecker>();
        sr = GetComponent<SpriteRenderer>();
        uiController = GameObject.FindObjectOfType<UIController>();
    }

    void Update()
    {
        canClimb = childrenClimbC.canClimb;

        if (!hasObjective && canPatrol){ Patrol(); }
        if (hasObjective) { FollowPlayer(); }

        if (isImmune) { DamageBlink(); }
    }

    void FollowPlayer()
    {
        canPatrol = false;

        //ROBOT ARRIBA
        //El robot esta arriba y el jugador a su derecha
        if (uSearchRight)
        {
            if (!lookRight)
            {
                ChangeDirectionY();
            }
            if (childrenGroundC.isGrounded)
            {
                transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta arriba y el jugador a su izquierda
        else if (uSearchLeft)
        {
            if (lookRight)
            {
                ChangeDirectionY();
            }
            if (childrenGroundC.isGrounded)
            {
                transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta arriba y el jugador en el lateral derecho
        else if (uSearchRightDown)
        {
            if (childrenGroundC.isGrounded || childrenClimbC.climb)
            {
                if (!lookRight)
                {
                    ChangeDirectionY();
                }
                if (isUp)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isRight)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenGroundC.isGrounded && isUp)
            {
                if (lookRight && canClimb)
                {
                    rotZ = 270;
                    //transform.rotation = Quaternion.Euler(0, 0, 270);
                    isUp = false;
                    isRight = true;
                }
                else if (!lookRight)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //El robot esta arriba y el jugador en el lateral izquierdo
        else if (uSearchLeftDown)
        {
            if (childrenGroundC.isGrounded || childrenClimbC.climb)
            {
                if (lookRight)
                {
                    ChangeDirectionY();
                }
                if (isUp)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isLeft)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenGroundC.isGrounded && isUp)
            {
                if (!lookRight && canClimb)
                {
                    rotZ = 270;
                    //transform.rotation = Quaternion.Euler(0, 180, 270);
                    isUp = false;
                    isLeft = true;
                }
                else if (lookRight)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //ROBOT ABAJO
        //El robot esta abajo y el jugador a su derecha
        if (dSearchRight)
        {
            if (lookRight)
            {
                ChangeDirectionY();
                //lookRight = false;
                //transform.rotation = Quaternion.Euler(0, 180, 180);
            }
            if (childrenClimbC.climb)
            {
                transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta abajo y el jugador a su izquierda
        else if (dSearchLeft)
        {
            if (!lookRight)
            {
                ChangeDirectionY();
                //lookRight = true;
                //transform.rotation = Quaternion.Euler(0, 0, 180);
            }
            if (childrenClimbC.climb)
            {
                transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta abajo y el jugador en el lateral derecho
        else if (dSearchRightUp)
        {
            if (childrenClimbC.climb)
            {
                if (lookRight)
                {
                    ChangeDirectionY();
                    //lookRight = false;
                    //transform.rotation = Quaternion.Euler(0, 180, 180);
                }
                if (isDown)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isRight)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenClimbC.climb && isDown)
            {
                if (!lookRight)
                {
                    rotZ = 90;
                    //transform.rotation = Quaternion.Euler(0, 180, 90);
                    isDown = false;
                    isRight = true;
                }
                else if (lookRight)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //El robot esta abajo y el jugador en el lateral izquierdo
        else if (dSearchLeftUp)
        {
            if (childrenClimbC.climb)
            {
                if (!lookRight)
                {
                    ChangeDirectionY();
                    //lookRight = false;
                    //transform.rotation = Quaternion.Euler(0, 0, 180);
                }
                if (isDown)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isLeft)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenClimbC.climb && isDown)
            {
                if (lookRight)
                {
                    rotZ = 90;
                    //transform.rotation = Quaternion.Euler(0, 0, 90);
                    isDown = false;
                    isLeft = true;
                }
                else if (!lookRight)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //ROBOT LATERAL DERECHO
        //El robot esta en el lateral derecho y el jugador encima
        if (rSearchUp)
        {
            rotZ = 90;
            if (lookRight)
            {
                ChangeDirectionY();
                //lookRight = false;
                //transform.rotation = Quaternion.Euler(0, 180, 90);
            }
            if (childrenClimbC.climb)
            {
                //lookRight = false;
                //transform.rotation = Quaternion.Euler(0, 180, 90);
                transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta en el lateral derecho y el jugador debajo
        else if (rSearchDown)
        {
            rotZ = 270;
            if (!lookRight)
            {
                ChangeDirectionY();
                //lookRight = true;
                //transform.rotation = Quaternion.Euler(0, 0, 270);
            }
            if (childrenClimbC.climb)
            {    
                //lookRight = true;
                //transform.rotation = Quaternion.Euler(0, 0, 270);
                transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta en el lateral derecho y el jugador arriba
        else if (rSearchUpLeft)
        {
            if (childrenGroundC.isGrounded || childrenClimbC.climb)
            {
                if (lookRight)
                {
                    ChangeDirectionY();
                    rotZ = 90;
                    //lookRight = false;
                    //transform.rotation = Quaternion.Euler(0, 180, 90);
                }
                if (isRight)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isUp)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenClimbC.climb && isRight)
            {
                if (!lookRight)
                {
                    rotZ = 0;
                    //transform.rotation = Quaternion.Euler(0, 180, 0);
                    isRight = false;
                    isUp = true;
                } 
                else if (lookRight)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //El robot esta en el lateral derecho y el jugador abajo
        else if (rSearchDownLeft)
        {
            if (childrenClimbC.climb)
            {
                if (!lookRight)
                {
                    ChangeDirectionY();
                    rotZ = 90;
                    //lookRight = true;
                    //transform.rotation = Quaternion.Euler(0, 0, 90);
                }
                if (isRight)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isDown)
                {
                    transform.Translate(Vector2.left * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenClimbC.climb && isRight)
            {
                if (lookRight)
                {
                    rotZ = 180;
                    //transform.rotation = Quaternion.Euler(0, 0, 180);
                    isRight = false;
                    isDown = true;
                }
                else if (!lookRight)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //ROBOT LATERAL IZQUIERDO
        //El robot esta en el lateral izquierdo y el jugador encima
        if (lSearchUp)
        {
            rotZ = 90;
            if (!lookRight)
            {
                ChangeDirectionY();
                //lookRight = true;
                //transform.rotation = Quaternion.Euler(0, 0, 90);
            }
            if (childrenClimbC.climb)
            {
                //lookRight = true;
                //transform.rotation = Quaternion.Euler(0, 0, 90);
                transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta en el lateral izquierdo y el jugador debajo
        else if (lSearchDown)
        {
            rotZ = 270;
            if (lookRight)
            {
                ChangeDirectionY();
                //lookRight = false;
                //transform.rotation = Quaternion.Euler(0, 180, 270);
            }
            if (childrenClimbC.climb)
            {
                //lookRight = false;
                //transform.rotation = Quaternion.Euler(0, 180, 270);
                transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
            }
        }

        //El robot esta en el lateral izquierdo y el jugador arriba
        else if (lSearchUpRight)
        {
            if (childrenGroundC.isGrounded || childrenClimbC.climb)
            {
                if (!lookRight)
                {
                    ChangeDirectionY();
                    rotZ = 90;
                    //lookRight = true;
                    //transform.rotation = Quaternion.Euler(0, 0, 90);
                }
                if (isLeft)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isUp)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }
            }
            else if (!childrenClimbC.climb && isLeft)
            {
                if (lookRight)
                {
                    rotZ = 0;
                    //transform.rotation = Quaternion.Euler(0, 0, 0);
                    isLeft = false;
                    isUp = true;
                }
                else if (!lookRight)
                {
                    transform.Translate(Vector2.up * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }

        //El robot esta en el lateral izquierdo y el jugador abajo
        else if (lSearchDownRight)
        {
            if (childrenClimbC.climb)
            {
                if (lookRight)
                {
                    ChangeDirectionY();
                    rotZ = 270;
                    //lookRight = false;
                    //transform.rotation = Quaternion.Euler(0, 180, 270);
                }
                if (isLeft)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
                else if (isDown)
                {
                    transform.Translate(Vector2.right * moveSpeed * Time.deltaTime, Space.World);
                }

            }
            else if (!childrenClimbC.climb && isLeft)
            {
                if (!lookRight)
                {
                    rotZ = 180;
                    //transform.rotation = Quaternion.Euler(0, 180, 180);
                    isLeft = false;
                    isDown = true;
                }
                else if (lookRight)
                {
                    transform.Translate(Vector2.down * moveSpeed * Time.deltaTime, Space.World);
                }
            }
        }
    }

    //Si no tiene vision del player patrulla.
    void Patrol()
    {
        if (lookRight)
        {
            transform.Translate(Vector2.right * patrolSpeed * Time.deltaTime, Space.World);
        }
        else
        {
            transform.Translate(Vector2.left * patrolSpeed * Time.deltaTime, Space.World);
        }

        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolTime)
        {
            patrolTimer = 0;
            ChangeDirectionY();
        }
    }

    //Rota para mirar hacia la derecha o la izquierda.
    void ChangeDirectionY()
    {
        if (lookRight)
        {
            rotY = 180;
            //transform.rotation = new Quaternion(0, 180, 0, 0);
            lookRight = false;
        }
        else
        {
            rotY = 0;
            //transform.rotation = new Quaternion(0, 0, 0, 0);
            lookRight = true;
        }
    }

    //Rotar para mirar hacia otra direccion.
    void ChangeDirectionZ()
    {
        if (faceDown) 
        {
            rotZ = 180;
            //transform.rotation = new Quaternion(0, 0, 180, 0);  
        }
        else
        {
            if (lookUp)
            {
                rotZ = 270;
                //transform.rotation = new Quaternion(0, 0, 270, 0);
                lookUp = false;
            }
            else
            {
                rotZ = 90;
                //transform.rotation = new Quaternion(0, 0, 90, 0);
                lookUp = true;
            }
        }
    }

    //Daña al robot al recibir disparos.
    void GetDamage(int dmg)
    {
        health -= dmg;
        //Añadir animación de daño

        if (health <= 0)
        {
            GameObject effect = Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(effect, 0.5f);
            uiController.DisabledEnemyCanvas();
        }
        else
        {
            isImmune = true;
            immuneTimer = 0;
            sr.enabled = false;
            blinkTimer = 0;
        }
    }

    //Si recibe daño, parpadea
    void DamageBlink()
    {
        immuneTimer += Time.deltaTime;
        blinkTimer += Time.deltaTime;

        if (blinkTimer >= blinkTime)
        {
            sr.enabled = !sr.enabled;
            blinkTimer = 0;
        }

        if (immuneTimer >= immuneTime)
        {
            sr.enabled = true;
            isImmune = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = true;
        }

        if (collision.transform.tag == "Projectile")
        {
            var dmg = collision.gameObject.GetComponent<Projectile>().damage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Attack")
        {
            var dmg = collision.gameObject.GetComponentInParent<CharacterController>().attackDamage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
        }

        if (collision.transform.tag == "Climb")
        {
            canClimb = true;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            //ROBOT ARRIBA
            //Comprobar si el robot esta arriba, el jugador a su derecha
            if (collision.transform.position.x > transform.position.x && collision.transform.position.y >= transform.position.y && isUp)
            {
                uSearchRight = true;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta arriba y el jugador a su izquierda
            if (collision.transform.position.x < transform.position.x && collision.transform.position.y >= transform.position.y && isUp)
            {
                uSearchRight = false;
                uSearchLeft = true;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta arriba y el jugador en el lateral derecho
            if (collision.transform.position.x >= transform.position.x && collision.transform.position.y < transform.position.y && isUp)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = true;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta arriba y el jugador en el lateral izquierdo
            if (collision.transform.position.x <= transform.position.x && collision.transform.position.y < transform.position.y && isUp)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = true;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //ROBOT ABAJO
            //Comprobar si el robot esta abajo, el jugador a su derecha
            if (collision.transform.position.x > transform.position.x && collision.transform.position.y <= transform.position.y && isDown)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = true;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta abajo y el jugador a su izquierda
            if (collision.transform.position.x < transform.position.x && collision.transform.position.y <= transform.position.y && isDown)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = true;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta abajo y el jugador en el lateral derecho
            if (collision.transform.position.x >= transform.position.x && collision.transform.position.y > transform.position.y && isDown)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = true;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta abajo y el jugador en el lateral izquierdo
            if (collision.transform.position.x <= transform.position.x && collision.transform.position.y > transform.position.y && isDown)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = true;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //ROBOT LATERAL DERECHO
            //Comprobar si el robot esta en el lateral derecho y el jugador esta encima
            if (collision.transform.position.y > transform.position.y && isRight)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp =  true;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral derecho y el jugador esta debajo
            if (collision.transform.position.y < transform.position.y && isRight)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = true;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral derecho y el jugador esta arriba
            if (collision.transform.position.y > transform.position.y && collision.transform.position.x < transform.position.x && isRight)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = true;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral derecho y el jugador esta abajo
            if (collision.transform.position.y < transform.position.y && collision.transform.position.x < transform.position.x && isRight)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = true;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //ROBOT LATERAL IZQUIERDO
            //Comprobar si el robot esta en el lateral izquierdo y el jugador esta encima
            if (collision.transform.position.y > transform.position.y && isLeft)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = true;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral izquierdo y el jugador esta debajo
            if (collision.transform.position.y < transform.position.y && isLeft)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = true;
                lSearchUpRight = false;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral izquierdo y el jugador esta arriba
            if (collision.transform.position.y > transform.position.y && collision.transform.position.x > transform.position.x && isLeft)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = true;
                lSearchDownRight = false;
            }

            //Comprobar si el robot esta en el lateral izquierdo y el jugador esta abajo
            if (collision.transform.position.y < transform.position.y && collision.transform.position.x > transform.position.x && isLeft)
            {
                uSearchRight = false;
                uSearchLeft = false;
                uSearchRightDown = false;
                uSearchLeftDown = false;
                dSearchRight = false;
                dSearchLeft = false;
                dSearchRightUp = false;
                dSearchLeftUp = false;
                rSearchUp = false;
                rSearchDown = false;
                rSearchUpLeft = false;
                rSearchDownLeft = false;
                lSearchUp = false;
                lSearchDown = false;
                lSearchUpRight = false;
                lSearchDownRight = true;
            }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = false;
            ChangeDirectionY();
        }
    }

    public float rotY
    {
        get
        {
            return transform.rotation.eulerAngles.y;
        }
        set
        {
            Vector3 v = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(v.x, value, v.z);
        }
    }

    public float rotZ
    {
        get
        {
            return transform.rotation.eulerAngles.z;
        }
        set
        {
            Vector3 v = transform.rotation.eulerAngles;
            transform.rotation = Quaternion.Euler(v.x, v.y, value);
        }
    }
}
