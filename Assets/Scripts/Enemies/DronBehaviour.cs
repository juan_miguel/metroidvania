﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class DronBehaviour : MonoBehaviour
{
    [Header("Dron Attributes")]
    public Sprite faceImage;
    public int health;
    public int maxHealth;
    public float patrolTime;
    private float patrolTimer;
    public float aimTime;
    private float aimingTimer;
    public float refreshTime;
    private float refreshTimer;
    public Collider2D bodyCollider;

    [Header("Sprites")]
    public GameObject eyesSprite;
    //public Color eyesColor3;
    public Color eyesColor2;
    public Color eyesColor1;
    public Color eyesColor0;
    public GameObject explosion;

    [Header("Animators")]
    public Animator dronAnimator;
    public Animator eyesAnimator;

    [Header("AI")]
    //public Transform[] searchPoints;
    private NavMeshAgent agent;
    public Transform playerTransform;
    private Vector3 initialPosition;

    [Header("UI")]
    public UIController uiController;

    [Header("Shoot")]
    public Transform shootPosition;
    public GameObject bullet;


    public bool hasObjective;
    public bool lookRight;
    public bool canShoot;
    public bool canPatrol;


    private void Start()
    {
        if (transform.rotation.y == 0) { lookRight = true; }
        else { lookRight = false; }

        agent = GetComponent<NavMeshAgent>();
        agent.updateRotation = false;
        agent.updateUpAxis = false;
        agent.autoBraking = false;

        initialPosition = transform.position;

        uiController = GameObject.FindObjectOfType<UIController>();
    }

    private void Update()
    {
        if (hasObjective) { FollowPlayer(); }
        if (!canShoot) { Refresh(); }
        if (!hasObjective && canPatrol) { Patrol(); }
        else if (canShoot) { Aim(); }

        CheckPosition();
        //CheckDirection();
    }

    //Si pierde vision del player lo busca.
    /*void SearchPlayer()
    {
        int nextPoint = Random.Range(0, searchPoints.Length);

        if (lookRight && searchPoints[nextPoint].position.x > transform.position.x)
        {
            agent.destination = searchPoints[nextPoint].position;
        }
        else if(!lookRight && searchPoints[nextPoint].position.x < transform.position.x)
        {
            agent.destination = searchPoints[nextPoint].position;
        }
        else
        {
            nextPoint = Random.Range(0, searchPoints.Length);
        }
    }*/

    //Comprobar si se encuentra en la posición inicial.
    void CheckPosition()
    {
        if (agent.remainingDistance < 0.1f)
        {
            agent.isStopped = true;
            canPatrol = true;   
        }
    }

    //Si tiene vision del player lo persigue.
    void FollowPlayer()
    {
        agent.isStopped = false;
        if (health > 0)
        {
            agent.destination = playerTransform.position;
            agent.stoppingDistance = 1f;
        }
    }

        //Si no tiene vision del player patrulla.
        void Patrol()
    {
        if(lookRight)
        {
            transform.Translate(Vector2.right * 0.3f * Time.deltaTime, Space.World);
        }
        else
        {
            transform.Translate(Vector2.left * 0.3f * Time.deltaTime, Space.World);
        }

        patrolTimer += Time.deltaTime;
        if (patrolTimer >= patrolTime) {
            patrolTimer = 0;
            ChangeDirection();
        }
    }

    //Si tiene vision del player apunta durante un tiempo y despues dispara.
    void Aim()
    {
        dronAnimator.Play("Aim");
        eyesAnimator.Play("Aim");

        aimingTimer += Time.deltaTime;
        if (aimingTimer >= aimTime)
        {
            aimingTimer = 0;
            dronAnimator.SetTrigger("Shoot");
            eyesAnimator.SetTrigger("Shoot");
            Shoot();
            canShoot = false;
        }
    }

    //Rota para mirar hacia la otra dirección.
    void ChangeDirection()
    {
        if (lookRight)
        {
            transform.rotation = new Quaternion(0, 180, 0, 0);
            eyesSprite.transform.rotation = new Quaternion(0, 180, 0, 0);
            lookRight = false;
        }
        else
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
            eyesSprite.transform.rotation = new Quaternion(0, 0, 0, 0);
            lookRight = true;
        }

        dronAnimator.Play("ChangeDirection");
        eyesAnimator.Play("ChangeDirection");
    }

    //Ejecuta el disparo.
    void Shoot()
    {
        Instantiate(bullet, shootPosition.position, shootPosition.rotation);
    }

    //Delay para reiniciar el funcionamiento del dron.
    void Refresh()
    {
        refreshTimer += Time.deltaTime;
        if (refreshTimer >= refreshTime)
        {
            refreshTimer = 0;
            canShoot = true;
        }
    }

    //Daña al dron al recibir disparos.
    void GetDamage(int dmg)
    {
        health -= dmg;
        ChangeEyesColor();

        if(health <= 0)
        {
            //Implementar muerte del dron
            GameObject effect = Instantiate(explosion, transform.position, transform.rotation);
            Destroy(gameObject);
            Destroy(effect, 0.5f);
            uiController.DisabledEnemyCanvas();
        }
    }

    //Cambia el color de los ojos en funcion de la salud restante.
    void ChangeEyesColor()
    {
        if (health == 2) { eyesSprite.GetComponent<SpriteRenderer>().color = eyesColor2; }
        if (health == 1) { eyesSprite.GetComponent<SpriteRenderer>().color = eyesColor1; }
        if (health == 0) { eyesSprite.GetComponent<SpriteRenderer>().color = eyesColor0; }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = true;
        }

        if (collision.transform.tag == "Projectile")
        {
            var dmg = collision.gameObject.GetComponent<Projectile>().damage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            GetDamage(dmg);
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Attack")
        {
            var dmg = collision.gameObject.GetComponentInParent<CharacterController>().attackDamage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            GetDamage(dmg);
        }

    }


    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = false;
            ChangeDirection();
            if(health > 0) { 
                agent.destination = initialPosition;
                agent.stoppingDistance = 0;
            }

            if (canShoot)
            {
                aimingTimer = 0;
                dronAnimator.Play("LostObjective");
                eyesAnimator.Play("LostObjective");
            }
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (collision.transform.position.x > transform.position.x)
            {
                if (!lookRight)
                {
                    ChangeDirection();
                }
            }
            else
            {
                if (lookRight)
                {
                    ChangeDirection();
                }
            }
        }
    }
}