﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MercenarioBehaviour : MonoBehaviour
{
    [Header("Mercenario Attributes")]
    public Sprite faceImage;
    public int health;
    public int maxHealth;
    public float aimTime;
    private float aimingTimer;
    public float refreshTime;
    private float refreshTimer;
    private float blinkTimer;
    public float blinkTime;
    private float immuneTimer;
    public float immuneTime;

    private SpriteRenderer sr;

    [Header("Animators")]
    public Animator mercenarioAnimator;

    [Header("IA")]
    public float playerStopDistance;
    public Transform playerTransform;

    [Header("UI")]
    public UIController uiController;

    [Header("Shoot")]
    public Transform shootPosition;
    public GameObject bullet;

    public bool isImmune;
    public bool hasObjective;
    public bool lookRight;
    public bool canShoot;

    void Start()
    {
        if (transform.rotation.y == 0) { lookRight = true; }
        else { lookRight = false; }

        sr = GetComponent<SpriteRenderer>();
        uiController = GameObject.FindObjectOfType<UIController>();
    }

    void Update()
    {
        if (hasObjective && !canShoot) { Refresh(); }
        else if (canShoot) { Aim(); }

        if (isImmune) { DamageBlink(); }
    }

    void Shoot()
    {
        Instantiate(bullet, shootPosition.position, shootPosition.rotation);
    }

    void Aim()
    {
        aimingTimer += Time.deltaTime;
        if (aimingTimer >= aimTime)
        {
            aimingTimer = 0;
            Shoot();
            canShoot = false;
        }
    }

    void ChangeDirection()
    {
        if (lookRight)
        {
            transform.rotation = new Quaternion(0, 180, 0, 0);
            lookRight = false;
        }
        else
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
            lookRight = true;
        }
    }

    void Refresh()
    {
        refreshTimer += Time.deltaTime;
        if (refreshTimer >= refreshTime)
        {
            refreshTimer = 0;
            canShoot = true;
        }
    }

    void GetDamage(int dmg)
    {
        health -= dmg;
        //Añadir animación de daño

        if (health <= 0)
        {
            mercenarioAnimator.SetTrigger("Dead");
            Destroy(gameObject, 1.0f);
            uiController.DisabledEnemyCanvas();
        }
        else
        {
            isImmune = true;
            immuneTimer = 0;
            sr.enabled = false;
            blinkTimer = 0;
        }
    }

    void DamageBlink()
    {
        immuneTimer += Time.deltaTime;
        blinkTimer += Time.deltaTime;

        if (blinkTimer >= blinkTime)
        {
            sr.enabled = !sr.enabled;
            blinkTimer = 0;
        }

        if (immuneTimer >= immuneTime)
        {
            sr.enabled = true;
            isImmune = false;
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            mercenarioAnimator.SetBool("Shoot", true);
            hasObjective = true;
        }

        if (collision.transform.tag == "Projectile")
        {
            var dmg = collision.gameObject.GetComponent<Projectile>().damage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Attack")
        {
            var dmg = collision.gameObject.GetComponentInParent<CharacterController>().attackDamage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            mercenarioAnimator.SetBool("Shoot", false);
            hasObjective = false;
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (collision.transform.position.x > transform.position.x)
            {
                if (!lookRight)
                {
                    ChangeDirection();
                }
            }
            else
            {
                if (lookRight)
                {
                    ChangeDirection();
                }
            }
        }
    }
}
