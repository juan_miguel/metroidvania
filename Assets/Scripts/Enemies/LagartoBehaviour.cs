﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LagartoBehaviour : MonoBehaviour
{
    [Header("Lagarto Attributes")]
    public Sprite faceImage;
    public int health;
    public int maxHealth;
    private float blinkTimer;
    public float blinkTime;
    private float immuneTimer;
    public float immuneTime;
    public float moveSpeed;
    public float attackTime;
    private float attackTimer;
    public float damageTime;

    private SpriteRenderer sr;

    [Header("Animators")]
    public Animator lagartoAnimator;

    [Header("Jump")]
    public float jumpWallForce;
    public bool isWall;

    [Header("IA")]
    public float playerStopDistance;
    public Transform playerTransform;
    private Rigidbody2D rb;
    private RobotWallChecker childrenWallC;
    private RobotGroundChecker childrenGroundC;

    [Header("UI")]
    public UIController uiController;

    public bool hasObjective;
    public bool lookRight;
    public bool canAttack;
    public bool isImmune;

    public bool canJump = true;
    public bool noGround;
    public bool isAttacking;

    void Start()
    {
        if (transform.rotation.y == 0) { lookRight = true; }
        else { lookRight = false; }

        childrenWallC = GetComponentInChildren<RobotWallChecker>();
        childrenGroundC = GetComponentInChildren<RobotGroundChecker>();
        rb = GetComponent<Rigidbody2D>();
        uiController = GameObject.FindObjectOfType<UIController>();
        sr = GetComponent<SpriteRenderer>();
    }

    void Update()
    {
        if (hasObjective) { FollowPlayer(); }
        if (hasObjective && canAttack) { Attack(); }
        if (isImmune) { Damage(); }

        if (childrenWallC.isWall && canJump )
        {
            isWall = true;
            Jump();
        }

        CheckGround();
    }

    void CheckGround()
    {
        if (childrenGroundC.isGrounded)
        {
            canJump = true;
            noGround = false;
        }
        else
        {
            noGround = true;
        }
    }

    void Jump()
    {
        canJump = false;

        if (isWall)
        {
            lagartoAnimator.SetTrigger("Jump");
            rb.velocity = Vector3.zero;
            rb.AddForce(new Vector2(0, jumpWallForce), ForceMode2D.Impulse);
            isWall = false;
            canAttack = false;
        }
    }

    

    void Attack()
    {
        if (!noGround && !lagartoAnimator.GetCurrentAnimatorStateInfo(0).IsName("Lagarto_Attack"))
        {
            lagartoAnimator.SetTrigger("Attack");
            StartCoroutine(jumpAttack());
            attackTimer += Time.deltaTime;
            if (attackTimer >= attackTime)
            {
                attackTimer = 0;
            }
        }
    }

    IEnumerator jumpAttack()
    {
        yield return new WaitForSeconds(0.4f);
        rb.velocity = Vector2.zero;
        rb.AddForce(new Vector2(0, 0.7f), ForceMode2D.Impulse);
    }

    void FollowPlayer()
    {
        if (Vector2.Distance(transform.position, playerTransform.position) > playerStopDistance)
        {
            canAttack = false;
            if (!noGround)
            {
                isAttacking = false;
            }
            lagartoAnimator.SetBool("Walk", true);
            transform.position = Vector2.MoveTowards(transform.position, new Vector2(playerTransform.position.x, transform.position.y), moveSpeed * Time.deltaTime);
        }
        else 
        {
            lagartoAnimator.SetBool("Walk", false);
            canAttack = true;
            canJump = false;
            isAttacking = true;
        }
    }

    public void ChangeDirection()
    {
        if (lookRight)
        {
            transform.rotation = new Quaternion(0, 180, 0, 0);
            lookRight = false;
        }
        else
        {
            transform.rotation = new Quaternion(0, 0, 0, 0);
            lookRight = true;
        }
    }

    void GetDamage(int dmg)
    {
        health -= dmg;
        
        hasObjective = false;

        if (health <= 0)
        {
            lagartoAnimator.SetTrigger("Dead");
            Destroy(gameObject, 0.8f);
            uiController.DisabledEnemyCanvas();
        }
        else 
        {
            lagartoAnimator.SetTrigger("Hurt");
            isImmune = true;
            immuneTimer = 0;
            sr.enabled = false;
            blinkTimer = 0;
        }
    }

    //Si recibe daño, parpadea
    void Damage()
    {
        immuneTimer += Time.deltaTime;
        blinkTimer += Time.deltaTime;

        if (blinkTimer >= blinkTime)
        {
            sr.enabled = !sr.enabled;
            blinkTimer = 0;
        }

        if (immuneTimer >= immuneTime)
        {
            sr.enabled = true;
            isImmune = false;
        }
    }
    
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            hasObjective = true;
        }
        
        if (collision.transform.tag == "Projectile")
        {
            var dmg = collision.gameObject.GetComponent<Projectile>().damage;
            
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
            Destroy(collision.gameObject);
        }

        if (collision.transform.tag == "Attack")
        {
            var dmg = collision.gameObject.GetComponentInParent<CharacterController>().attackDamage;
            uiController.EnabledEnemyCanvas(health, dmg, maxHealth, gameObject.name, faceImage);
            if (!isImmune) { GetDamage(dmg); }
        }
    }
    
    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            lagartoAnimator.SetBool("Walk", false);
            hasObjective = false;
            ChangeDirection();
        }
    }

    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.transform.tag == "Player")
        {
            if (collision.transform.position.x > transform.position.x)
            {
                if (!lookRight)
                {
                    ChangeDirection();
                }
            }
            else
            {
                if (lookRight)
                {
                    ChangeDirection();
                }
            }
        }
    }
}
