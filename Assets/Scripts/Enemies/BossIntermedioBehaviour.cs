﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossIntermedioBehaviour : MonoBehaviour
{
    [Header("Boss Intermedio Attributes")]
    public Sprite faceImage;
    public int EnemyHealth;
    public int maxHealth;

    [Header("Animators")]
    public Animator BossIntermedioAnimator;

    public float phaseChangeThreshold = 100f; // Umbral para cambiar a la fase 2 (50% de vida)

    private bool phaseTwo = false;
    private bool canAttack = true;
    private float attackCooldown = 3f; // Tiempo de espera después de cada ataque
    private float phaseCooldown = 4f; // Tiempo antes de cambiar de fase NO SE ESTÁ USANDO

    

    private void Update()
    {
        if (GetComponent<EnemyHealth>().GetCurrentHealth() <= GetComponent<EnemyHealth>().GetMaxHealth() * phaseChangeThreshold)
        {
            if (!phaseTwo)
            {
                // Cambiar a fase 2
                phaseTwo = true;
                Debug.Log("Fase 2 activada");
                attackCooldown = 3f; // Reiniciar el tiempo de espera después de cada ataque para la fase 2
                phaseCooldown = 3f; // Reducir el tiempo antes de cambiar de fase para la fase 2
            }
        }

        if (canAttack)
        {
            if (!phaseTwo)
            {
                // Ataques para la fase 1
                MeleeAttack1();
                MeleeAttack2();
                RangedAttack();
            }
            else
            {
                // Ataques para la fase 2
                MeleeAttack2Phase2();
                RangedCombo();
                MeleeAttack1Phase2();
            }
        }
    }

    private void MeleeAttack1()
    {
        // Lógica de la animación y el ataque Melee1 para la fase 1
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque Melee1 fase 1");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void MeleeAttack2()
    {
        // Lógica de la animación y el ataque Melee2 para la fase 1
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque Melee2 fase 1");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void RangedAttack()
    {
        // Lógica de la animación y el ataque a distancia para la fase 1
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque a distancia fase 1");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void MeleeAttack1Phase2()
    {
        // Lógica de la animación y el ataque Melee1 para la fase 2
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque Melee1 fase 2");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void MeleeAttack2Phase2()
    {
        // Lógica de la animación y el ataque Melee2 para la fase 2
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque Melee2 fase 2");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void RangedCombo()
    {
        // Lógica de la animación y el ataque a distancia en combo para la fase 2
        // Coloca la lógica de daño al jugador aquí
        Debug.Log("Ataque a distancia en combo fase 2");
        canAttack = false;
        Invoke("EnableAttack", attackCooldown);
    }

    private void EnableAttack()
    {
        // Permitir que el boss ataque nuevamente después del tiempo de espera
        canAttack = true;
    }
}