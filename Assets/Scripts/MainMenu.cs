﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour
{

    public void NewGame()
    {
        SceneManager.LoadScene("Zone1");

    }

    public void Continue()
    {
        Debug.Log("continuo con la partida");
    }

    public void Options()
    {
        Debug.Log("voy a las opciones");
    }
}
